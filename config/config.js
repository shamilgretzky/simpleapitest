var argv = require('minimist')(process.argv.slice(2));
var configFile = require(__dirname + '/config.json');


var config = configFile[process.env.NODE_ENV || argv.env || "qa"];

module.exports = config;
