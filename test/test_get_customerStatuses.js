//define some variables
var should = require("should");
var request = require("request");
var expect = require("chai").expect;
var util = require("util");

const config = require("../config/config");

let paths = require("../resources/paths.json");
let parameters = require("../resources/parameters.json");
let baseUrl = config.baseUrl;

describe('Test Get Customer Statues', function(){
  describe('Test Get All Customer Statues end point', function() {
    describe("Positive Cases", function() {
        it('Should Return all Customer Statues in JSON format', function(done){
          request.get({url: baseUrl + paths.customerStatuses}, function(error, response, body) {
          //console.log(baseUrl);
          var bodyObj = JSON.parse(body);
          expect(response.statusCode).to.equal(200);
          //console.log(body);
          done();
          });
        }).timeout(0);

        it('Returns all Customer Statuses with status response 200', function(done){
          request.get({url: baseUrl + paths.customerStatuses}, function(error, response, body) {
          expect(response.statusCode).to.equal(200);
          //console.log(body);
          done();
          });
        }).timeout(0);
    }); //Close Positive describe

    describe("Negative Cases", function(){
          it("Should return status response 404 for passing an invalid path",function(done){
            request.get({url: baseUrl + paths.invalidCustomerStatuses}, function(error, response, body) {
            //console.log(body);
            expect(response.statusCode).to.equal(404);
            //console.log(body);
            done();
            });
          }).timeout(0);
    });
  });

  describe('Get Customer Statues by ID endpoint (/customer-statuses/{ID})', function() {
    describe("Positive Cases", function() {
        it('Should Return corresponding Customer Statues when passing specific id', function(done){
          request.get({url: baseUrl + paths.customerStatuses + parameters.customerStatuses.Customer1.id}, function(error, response, body) {
            var bodyObj = JSON.parse(body);
            expect(bodyObj.id).to.equal(parameters.customerStatuses.Customer1.id);
            expect(response.statusCode).to.equal(200);
            //console.log(body);
            done();
          });
        }).timeout(0);

        it('Should return correct Customer Statues name in the response body', function(done){
          request.get({url: baseUrl + paths.customerStatuses + parameters.customerStatuses.Customer1.id}, function(error, response, body) {
            var bodyObj = JSON.parse(body);
            expect(bodyObj.name).to.equal(parameters.customerStatuses.Customer1.name);
            expect(response.statusCode).to.equal(200);
            //console.log(body);
            done();
          });
        }).timeout(0);
      });

      describe("Negative Cases", function() {
        it('Should return null (Passing Invalid Parameter for Customer Statues)', function(done){
          request.get({url: baseUrl + paths.customerStatuses + parameters.customerStatuses.InvalidCustomer.id}, function(error, response, body) {
            var bodyObj = JSON.parse(body);
            expect(bodyObj).to.not.equal(parameters.customerStatuses.InvalidCustomer.id);
            expect(response.statusCode).to.equal(200);
            //console.log(body);
            done();
          });
        }).timeout(0);
      });
  });
});
