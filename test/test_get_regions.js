//define some variables
var should = require("should");
var request = require("request");
var expect = require("chai").expect;
var util = require("util");

const config = require("../config/config");

let paths = require("../resources/paths.json");
let parameters = require("../resources/parameters.json");
let baseUrl = config.baseUrl;

describe('Test Get Regions', function(){
  describe('Test Get All Regions end point', function() {
    describe("Positive Cases", function() {
        it('Should Return all Regions in JSON format', function(done){
          request.get({url: baseUrl + paths.regions}, function(error, response, body) {
          //console.log(baseUrl);
          var bodyObj = JSON.parse(body);
          expect(response.statusCode).to.equal(200);
          //console.log(body);
          done();
          });
        }).timeout(0);

        it('Returns all Regions with status response 200', function(done){
          request.get({url: baseUrl + paths.regions}, function(error, response, body) {
          expect(response.statusCode).to.equal(200);
          //console.log(body);
          done();
          });
        }).timeout(0);
    }); //Close Positive describe

    describe("Negative Cases", function(){
          it("Should return status response 404 for passing an invalid path",function(done){
            request.get({url: baseUrl + paths.invalidRegions}, function(error, response, body) {
            //console.log(body);
            expect(response.statusCode).to.equal(404);
            //console.log(body);
            done();
            });
          }).timeout(0);
    });
  });

  describe('Get Regions by ID endpoint (/regions/{ID})', function() {
    describe("Positive Cases", function() {
        it('Should Return corresponding Regions when passing specific id', function(done){
          request.get({url: baseUrl + paths.regions + parameters.Regions.Quebec.id}, function(error, response, body) {
            var bodyObj = JSON.parse(body);
            expect(bodyObj.id).to.equal(parameters.Regions.Quebec.id);
            expect(response.statusCode).to.equal(200);
            //console.log(body);
            done();
          });
        }).timeout(0);

        it('Should return correct Regions name in the response body', function(done){
          request.get({url: baseUrl + paths.regions + parameters.Regions.Quebec.id}, function(error, response, body) {
            var bodyObj = JSON.parse(body);
            expect(bodyObj.name).to.equal(parameters.Regions.Quebec.name);
            expect(response.statusCode).to.equal(200);
            //console.log(body);
            done();
          });
        }).timeout(0);

        it('Should return correct Regions country in the response body', function(done){
          request.get({url: baseUrl + paths.regions + parameters.Regions.Quebec.id}, function(error, response, body) {
            var bodyObj = JSON.parse(body);
            expect(bodyObj.country).to.equal(parameters.Regions.Quebec.country);
            expect(response.statusCode).to.equal(200);
            //console.log(body);
            done();
          });
        }).timeout(0);
      });

      describe("Negative Cases", function() {
        it('Should return null (Passing Invalid Parameter for Regions)', function(done){
          request.get({url: baseUrl + paths.regions + parameters.Regions.InvalidRegion.id}, function(error, response, body) {
            var bodyObj = JSON.parse(body);
            expect(bodyObj).to.not.equal(parameters.Regions.InvalidRegion.id);
            expect(response.statusCode).to.equal(200);
            //console.log(body);
            done();
          });
        }).timeout(0);
      });
  });
});
