//USE MOCHA --reporter mochawsome  to test and get results in html

/*

//define some variables
var should = require("should");
var request = require("request");
var expect = require("chai").expect;
var util = require("util");
const config = require("../config/config");
//const config = require("../config/config.json");
//var baseUrl = "http://exp-lookup.ca-c1.cloudhub.io/api"
//const apiEndpointHelper = require('../helper/api_endpoints.js');
//const testData = require('../helper/test_data.js');
//const validator = require('../helper/validators.js');
//const config = require('../config/config.json');

//var valid_country = "CA";

//let idCountry = testData.id;
//let nameCountry = testData.name;
let paths = require("../resources/paths.json");
let parameters = require("../resources/parameters.json");
let baseUrl = config.baseUrl;
//let baseUrl = apiEndpointHelper.baseUrl;


let countriesEndpoint = apiEndpointHelper.countriesApiEndPoint;
let CAEndpoint = apiEndpointHelper.CAApiEndPoint;
let BREndpoint = apiEndpointHelper.BRApiEndPoint;
let BEEndpoint = apiEndpointHelper.BEApiEndPoint;
let negCountry404 = apiEndpointHelper.negcountryApiEndPoint;
let nullCountry = apiEndpointHelper.ENNULLEndpoint;

*/


/*
describe('Should return all countries ID and Name', function() {
  describe("Positive Cases Get Countries", function() {
  it('Returns all countries ID and Name in JSON format', function(done){
      request.get({url: baseUrl + paths.countries}, function(error, response, body) {
        //console.log(baseUrl);
       var bodyObj = JSON.parse(body);
       expect(response.statusCode).to.equal(200);
      //console.log(body);
      done();
    });
  }).timeout(0);


  it('Returns all countries ID and Name with status response 200', function(done){
      request.get({url: baseUrl + paths.countries}, function(error, response, body) {
      expect(response.statusCode).to.equal(200);
      //console.log(body);
      done();
    });
  }).timeout(0);
}); //Close Positive describe


  describe("Negative Cases Get Countries", function(){
    it("Status Response 404",function(done){
      request.get({url: baseUrl + paths.invalidCountry}, function(error, response, body) {
        //console.log(body);
      expect(response.statusCode).to.equal(404);
      //console.log(body);
      done();
      });
    }).timeout(0);

  });

});

describe('Should return Country by id & name', function() {
  it('Returns Country by id', function(done){ //removed done
    request.get({url: baseUrl + paths.countries + parameters.Countries.Canada.id}, function(error, response, body) {
      var bodyObj = JSON.parse(body);
      expect(bodyObj.id).to.equal(parameters.Countries.Canada.id);
      expect(response.statusCode).to.equal(200);
      //console.log(body);
      done();
    });
  }).timeout(0);

  it('Returns Country by name', function(done){
    request.get({url: baseUrl + paths.countries + parameters.Countries.Canada.id}, function(error, response, body) {
      var bodyObj = JSON.parse(body);
      expect(bodyObj.name).to.equal(parameters.Countries.Canada.name);
      expect(response.statusCode).to.equal(200);
      //console.log(body);
      done();
    });
  }).timeout(0);

});

describe('Should return null (Passing Invalid Parameter for Country)', function() {
  it('Returns null', function(done){
    request.get({url: baseUrl + paths.countries + parameters.Countries.InvalidCountry.id}, function(error, response, body) {
      var bodyObj = JSON.parse(body);
      expect(bodyObj).to.not.equal(parameters.Countries.InvalidCountry.id);
      expect(response.statusCode).to.equal(200);
      //console.log(body);
      done();
    });
  }).timeout(0);
});

describe('Should return all Region ID, Name and Countries', function() {
  describe("Positive Cases Get Region ID, Name and Countries", function() {
  it('Returns all Region ID, Name and Countries in JSON format', function(done){
      request.get({url: baseUrl + paths.regions}, function(error, response, body) {
        //console.log(baseUrl);
       var bodyObj = JSON.parse(body);
       expect(response.statusCode).to.equal(200);
      //console.log(body);
      done();
    });
  }).timeout(0);


  it('Returns all Region ID, Name and Countries with status response 200', function(done){
      request.get({url: baseUrl + paths.regions}, function(error, response, body) {
      expect(response.statusCode).to.equal(200);
      //console.log(body);
      done();
    });
  }).timeout(0);
}); //Close Positive describe


  describe("Negative Cases Get Regions", function(){
    it("Status Response 404",function(done){
      request.get({url: baseUrl + paths.invalidRegions}, function(error, response, body) {
        //console.log(body);
      expect(response.statusCode).to.equal(404);
      //console.log(body);
      done();
      });
    }).timeout(0);

  });
});

describe('Should return Region by id, name and country', function() {
  it('Returns Region by id', function(done){
    request.get({url: baseUrl + paths.regions + parameters.Regions.Quebec.id}, function(error, response, body) {
      var bodyObj = JSON.parse(body);
      expect(bodyObj.id).to.equal(parameters.Regions.Quebec.id);
      expect(response.statusCode).to.equal(200);
      //console.log(body);
      done();
    });
  }).timeout(0);

  it('Returns Region by name', function(done){
    request.get({url: baseUrl + paths.regions + parameters.Regions.Quebec.id}, function(error, response, body) {
      var bodyObj = JSON.parse(body);
      expect(bodyObj.name).to.equal(parameters.Regions.Quebec.name);
      expect(response.statusCode).to.equal(200);
      //console.log(body);
      done();
    });
  }).timeout(0);

  it('Returns Region by country', function(done){
    request.get({url: baseUrl + paths.regions + parameters.Regions.Quebec.id}, function(error, response, body) {
      var bodyObj = JSON.parse(body);
      expect(bodyObj.country).to.equal(parameters.Regions.Quebec.country);
      expect(response.statusCode).to.equal(200);
      //console.log(body);
      done();
    });
  }).timeout(0);

});

describe('Should return null (Passing Invalid Parameter for Region)', function() {
  it('Returns null', function(done){
    request.get({url: baseUrl + paths.regions + parameters.Regions.InvalidRegion.id}, function(error, response, body) {
      var bodyObj = JSON.parse(body);
      expect(bodyObj).to.not.equal(parameters.Regions.InvalidRegion.id);
      expect(response.statusCode).to.equal(200);
      //console.log(body);
      done();
    });
  }).timeout(0);
});

describe('Should return all Customer Types', function() {
  describe("Positive Cases Get Customer Types", function() {
  it('Returns all Customer Types in JSON format', function(done){
      request.get({url: baseUrl + paths.customerTypes}, function(error, response, body) {
        //console.log(baseUrl);
       var bodyObj = JSON.parse(body);
       expect(response.statusCode).to.equal(200);
      //console.log(body);
      done();
    });
  }).timeout(0);


  it('Returns all Customer Types with status response 200', function(done){
      request.get({url: baseUrl + paths.customerTypes}, function(error, response, body) {
      expect(response.statusCode).to.equal(200);
      //console.log(body);
      done();
    });
  }).timeout(0);
}); //Close Positive describe


  describe("Negative Cases Get Customer Types", function(){
    it("Status Response 404",function(done){
      request.get({url: baseUrl + paths.invalidCustomerTypes}, function(error, response, body) {
        //console.log(body);
      expect(response.statusCode).to.equal(404);
      //console.log(body);
      done();
      });
    }).timeout(0);

  });
});

describe('Should return Customer Types by id and name', function() {
  it('Returns Customer Types by id', function(done){
    request.get({url: baseUrl + paths.customerTypes + parameters.customerTypes.Type2.id}, function(error, response, body) {
      var bodyObj = JSON.parse(body);
      expect(bodyObj.id).to.equal(parameters.customerTypes.Type2.id);
      expect(response.statusCode).to.equal(200);
      //console.log(body);
      done();
    });
  }).timeout(0);

  it('Returns Customer Types by name', function(done){
    request.get({url: baseUrl + paths.customerTypes + parameters.customerTypes.Type2.id}, function(error, response, body) {
      var bodyObj = JSON.parse(body);
      expect(bodyObj.name).to.equal(parameters.customerTypes.Type2.name);
      expect(response.statusCode).to.equal(200);
      //console.log(body);
      done();
    });
  }).timeout(0);

});

describe('Should return null (Passing Invalid Parameter for Customer Types)', function() {
  it('Returns null', function(done){
    request.get({url: baseUrl + paths.customerTypes + parameters.customerTypes.InvalidType.id}, function(error, response, body) {
      var bodyObj = JSON.parse(body);
      expect(bodyObj).to.not.equal(parameters.customerTypes.InvalidType.id);
      expect(response.statusCode).to.equal(200);
      //console.log(body);
      done();
    });
  }).timeout(0);
});
//------------------------------------------------------------------------------------

describe('Should return all Languages', function() {
  describe("Positive Cases Get Languages", function() {
  it('Returns all Languages in JSON format', function(done){
      request.get({url: baseUrl + paths.languages}, function(error, response, body) {
        //console.log(baseUrl);
       var bodyObj = JSON.parse(body);
       expect(response.statusCode).to.equal(200);
      //console.log(body);
      done();
    });
  }).timeout(0);


  it('Returns all Languages with status response 200', function(done){
      request.get({url: baseUrl + paths.languages}, function(error, response, body) {
      expect(response.statusCode).to.equal(200);
      //console.log(body);
      done();
    });
  }).timeout(0);
}); //Close Positive describe


  describe("Negative Cases Get Languages", function(){
    it("Status Response 404",function(done){
      request.get({url: baseUrl + paths.invalidLanguages}, function(error, response, body) {
        //console.log(body);
      expect(response.statusCode).to.equal(404);
      //console.log(body);
      done();
      });
    }).timeout(0);

  });
});

describe('Should return Languages by id and name', function() {
  it('Returns Languages by id', function(done){
    request.get({url: baseUrl + paths.languages + parameters.languages.English.id}, function(error, response, body) {
      var bodyObj = JSON.parse(body);
      expect(bodyObj.id).to.equal(parameters.languages.English.id);
      expect(response.statusCode).to.equal(200);
      //console.log(body);
      done();
    });
  }).timeout(0);

  it('Returns Languages by name', function(done){
    request.get({url: baseUrl + paths.languages + parameters.languages.English.id}, function(error, response, body) {
      var bodyObj = JSON.parse(body);
      expect(bodyObj.name).to.equal(parameters.languages.English.name);
      expect(response.statusCode).to.equal(200);
      //console.log(body);
      done();
    });
  }).timeout(0);

});

describe('Should return null (Passing Invalid Parameter for Languages)', function() {
  it('Returns null', function(done){
    request.get({url: baseUrl + paths.languages + parameters.languages.InvalidLang.id}, function(error, response, body) {
      var bodyObj = JSON.parse(body);
      expect(bodyObj).to.not.equal(parameters.languages.InvalidLang.id);
      expect(response.statusCode).to.equal(200);
      //console.log(body);
      done();
    });
  }).timeout(0);
});
//------------------------------------------------------------------------------------------------
describe('Should return all Customer Statues', function() {
  describe("Positive Cases all Customer Statues", function() {
  it('Returns all Customer Statues in JSON format', function(done){
      request.get({url: baseUrl + paths.customerStatuses}, function(error, response, body) {
        //console.log(baseUrl);
       var bodyObj = JSON.parse(body);
       expect(response.statusCode).to.equal(200);
      //console.log(body);
      done();
    });
  }).timeout(0);


  it('Returns all Customer Statues with status response 200', function(done){
      request.get({url: baseUrl + paths.customerStatuses}, function(error, response, body) {
      expect(response.statusCode).to.equal(200);
      //console.log(body);
      done();
    });
  }).timeout(0);
}); //Close Positive describe


  describe("Negative Cases Get Customer Statues", function(){
    it("Status Response 404",function(done){
      request.get({url: baseUrl + paths.invalidCustomerStatuses}, function(error, response, body) {
        //console.log(body);
      expect(response.statusCode).to.equal(404);
      //console.log(body);
      done();
      });
    }).timeout(0);

  });
});

describe('Should return Customer Statues by id and name', function() {
  it('Returns Customer Statues by id', function(done){
    request.get({url: baseUrl + paths.customerStatuses + parameters.customerStatuses.Customer1.id}, function(error, response, body) {
      var bodyObj = JSON.parse(body);
      expect(bodyObj.id).to.equal(parameters.customerStatuses.Customer1.id);
      expect(response.statusCode).to.equal(200);
      //console.log(body);
      done();
    });
  }).timeout(0);

  it('Returns Customer Statues by name', function(done){
    request.get({url: baseUrl + paths.customerStatuses + parameters.customerStatuses.Customer1.id}, function(error, response, body) {
      var bodyObj = JSON.parse(body);
      expect(bodyObj.name).to.equal(parameters.customerStatuses.Customer1.name);
      expect(response.statusCode).to.equal(200);
      //console.log(body);
      done();
    });
  }).timeout(0);

});

describe('Should return null (Passing Invalid Parameter for Customer Statues)', function() {
  it('Returns null', function(done){
    request.get({url: baseUrl + paths.customerStatuses + parameters.customerStatuses.InvalidCustomer.id}, function(error, response, body) {
      var bodyObj = JSON.parse(body);
      expect(bodyObj).to.not.equal(parameters.customerStatuses.InvalidCustomer.id);
      expect(response.statusCode).to.equal(200);
      //console.log(body);
      done();
    });
  }).timeout(0);
});
//-----------------------------------------------------------------------

describe('Should return all Payment Methods', function() {
  describe("Positive Cases Get Payment Methods", function() {
  it('Returns all Payment Methods in JSON format', function(done){
      request.get({url: baseUrl + paths.paymentMethods}, function(error, response, body) {
        //console.log(baseUrl);
       var bodyObj = JSON.parse(body);
       expect(response.statusCode).to.equal(200);
      //console.log(body);
      done();
    });
  }).timeout(0);


  it('Returns all Payment Methods with status response 200', function(done){
      request.get({url: baseUrl + paths.paymentMethods}, function(error, response, body) {
      expect(response.statusCode).to.equal(200);
      //console.log(body);
      done();
    });
  }).timeout(0);
}); //Close Positive describe


  describe("Negative Cases Get Payment Methods", function(){
    it("Status Response 404",function(done){
      request.get({url: baseUrl + paths.invalidPaymentMethods}, function(error, response, body) {
        //console.log(body);
      expect(response.statusCode).to.equal(404);
      //console.log(body);
      done();
      });
    }).timeout(0);

  });
});

describe('Should return Payment Methods by id and name', function() {
  it('Returns Payment Methods by id', function(done){
    request.get({url: baseUrl + paths.paymentMethods + parameters.paymentMethods.Payment1.id}, function(error, response, body) {
      var bodyObj = JSON.parse(body);
      expect(bodyObj.id).to.equal(parameters.paymentMethods.Payment1.id);
      expect(response.statusCode).to.equal(200);
      //console.log(body);
      done();
    });
  }).timeout(0);

  it('Returns Payment Methods by name', function(done){
    request.get({url: baseUrl + paths.paymentMethods + parameters.paymentMethods.Payment1.id}, function(error, response, body) {
      var bodyObj = JSON.parse(body);
      expect(bodyObj.name).to.equal(parameters.paymentMethods.Payment1.name);
      expect(response.statusCode).to.equal(200);
      //console.log(body);
      done();
    });
  }).timeout(0);

});

describe('Should return null (Passing Invalid Parameter for Payment Methods)', function() {
  it('Returns null', function(done){
    request.get({url: baseUrl + paths.paymentMethods + parameters.paymentMethods.InvalidPayment.id}, function(error, response, body) {
      var bodyObj = JSON.parse(body);
      expect(bodyObj).to.not.equal(parameters.paymentMethods.InvalidPayment.id);
      expect(response.statusCode).to.equal(200);
      //console.log(body);
      done();
    });
  }).timeout(0);
});

//-----------------------------


describe('Should not return all Sales Representatives', function() {
  it('Returns all Sales Representatives in JSON format', function(done){
      request.get({url: baseUrl + paths.salesPerson}, function(error, response, body) {
        //console.log(baseUrl);
       var bodyObj = JSON.parse(body);
       //expect(response.statusCode).to.equal(200);
      //console.log(body);
      done();
    });
  }).timeout(0);


  it('Returns all Sales Representatives with status response 404', function(done){
      request.get({url: baseUrl + paths.salesPerson}, function(error, response, body) {
      expect(response.statusCode).to.equal(404);
      //console.log(body);
      done();
    });
  }).timeout(0);

});

describe('Should return Error message: Resource not found for Sales Representatives', function() {
  it('Returns Error message Resource not found', function(done){
    request.get({url: baseUrl + paths.salesPerson}, function(error, response, body) {
      var bodyObj = JSON.parse(body);
      expect(bodyObj.message).to.equal(parameters.salesPerson.NotFound.message);
      expect(response.statusCode).to.equal(404);
      //console.log(body);
      done();
    });
  }).timeout(0);

});
*/
