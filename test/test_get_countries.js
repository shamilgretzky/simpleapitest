//define some variables
var should = require("should");
var request = require("request");
var expect = require("chai").expect;
var util = require("util");

const config = require("../config/config");

let paths = require("../resources/paths.json");
let parameters = require("../resources/parameters.json");
let baseUrl = config.baseUrl;
describe('Test Get Countries', function(){
  describe('Test Get All Countries end point', function() {
    describe("Positive Cases", function() {
        it('Should Return all countries ID and Name in JSON format', function(done){
          request.get({url: baseUrl + paths.countries}, function(error, response, body) {
          //console.log(baseUrl);
          var bodyObj = JSON.parse(body);
          expect(response.statusCode).to.equal(200);
          //console.log(body);
          done();
          });
        }).timeout(0);

        it('Returns all countries ID and Name with status response 200', function(done){
          request.get({url: baseUrl + paths.countries}, function(error, response, body) {
          expect(response.statusCode).to.equal(200);
          //console.log(body);
          done();
          });
        }).timeout(0);
    }); //Close Positive describe

    describe("Negative Cases", function(){
          it("Should return status response 404 for passing an invalid path",function(done){
            request.get({url: baseUrl + paths.invalidCountry}, function(error, response, body) {
            //console.log(body);
            expect(response.statusCode).to.equal(404);
            //console.log(body);
            done();
            });
          }).timeout(0);
    });
  });

  describe('Get Country by ID endpoint (/countries/{ID})', function() {
    describe("Positive Cases", function() {
        it('Should Return corresponding Country when passing specific id', function(done){
          request.get({url: baseUrl + paths.countries + parameters.Countries.Canada.id}, function(error, response, body) {
            var bodyObj = JSON.parse(body);
            expect(bodyObj.id).to.equal(parameters.Countries.Canada.id);
            expect(response.statusCode).to.equal(200);
            //console.log(body);
            done();
          });
        }).timeout(0);

        it('Should return correct Country name in the response body', function(done){
          request.get({url: baseUrl + paths.countries + parameters.Countries.Canada.id}, function(error, response, body) {
            var bodyObj = JSON.parse(body);
            expect(bodyObj.name).to.equal(parameters.Countries.Canada.name);
            expect(response.statusCode).to.equal(200);
            //console.log(body);
            done();
          });
        }).timeout(0);
      });

      describe("Negative Cases", function() {
        it('Should return null (Passing Invalid Parameter for Country)', function(done){
          request.get({url: baseUrl + paths.countries + parameters.Countries.InvalidCountry.id}, function(error, response, body) {
            var bodyObj = JSON.parse(body);
            expect(bodyObj).to.not.equal(parameters.Countries.InvalidCountry.id);
            expect(response.statusCode).to.equal(200);
            //console.log(body);
            done();
          });
        }).timeout(0);
      });
  });
});
