//define some variables
var should = require("should");
var request = require("request");
var expect = require("chai").expect;
var util = require("util");

const config = require("../config/config");

let paths = require("../resources/paths.json");
let parameters = require("../resources/parameters.json");
let baseUrl = config.baseUrl;

describe('Test Get Customer Types', function(){
  describe('Test Get All Customer Types end point', function() {
    describe("Positive Cases", function() {
        it('Should Return all Customer Types in JSON format', function(done){
          request.get({url: baseUrl + paths.customerTypes}, function(error, response, body) {
          //console.log(baseUrl);
          var bodyObj = JSON.parse(body);
          expect(response.statusCode).to.equal(200);
          //console.log(body);
          done();
          });
        }).timeout(0);

        it('Returns all Customer Types with status response 200', function(done){
          request.get({url: baseUrl + paths.customerTypes}, function(error, response, body) {
          expect(response.statusCode).to.equal(200);
          //console.log(body);
          done();
          });
        }).timeout(0);
    }); //Close Positive describe

    describe("Negative Cases", function(){
          it("Should return status response 404 for passing an invalid path",function(done){
            request.get({url: baseUrl + paths.invalidCustomerTypes}, function(error, response, body) {
            //console.log(body);
            expect(response.statusCode).to.equal(404);
            //console.log(body);
            done();
            });
          }).timeout(0);
    });
  });

  describe('Get Customer Types by ID endpoint (/customer-types/{ID})', function() {
    describe("Positive Cases", function() {
        it('Should Return corresponding Customer Types when passing specific id', function(done){
          request.get({url: baseUrl + paths.customerTypes + parameters.customerTypes.Type2.id}, function(error, response, body) {
            var bodyObj = JSON.parse(body);
            expect(bodyObj.id).to.equal(parameters.customerTypes.Type2.id);
            expect(response.statusCode).to.equal(200);
            //console.log(body);
            done();
          });
        }).timeout(0);

        it('Should return correct Customer Types name in the response body', function(done){
          request.get({url: baseUrl + paths.customerTypes + parameters.customerTypes.Type2.id}, function(error, response, body) {
            var bodyObj = JSON.parse(body);
            expect(bodyObj.name).to.equal(parameters.customerTypes.Type2.name);
            expect(response.statusCode).to.equal(200);
            //console.log(body);
            done();
          });
        }).timeout(0);
      });

      describe("Negative Cases", function() {
        it('Should return null (Passing Invalid Parameter for Customer Types)', function(done){
          request.get({url: baseUrl + paths.customerTypes + parameters.customerTypes.InvalidType.id}, function(error, response, body) {
            var bodyObj = JSON.parse(body);
            expect(bodyObj).to.not.equal(parameters.customerTypes.InvalidType.id);
            expect(response.statusCode).to.equal(200);
            //console.log(body);
            done();
          });
        }).timeout(0);
      });
  });
});
