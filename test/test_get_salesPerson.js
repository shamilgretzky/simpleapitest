//define some variables
var should = require("should");
var request = require("request");
var expect = require("chai").expect;
var util = require("util");

const config = require("../config/config");

let paths = require("../resources/paths.json");
let parameters = require("../resources/parameters.json");
let baseUrl = config.baseUrl;

describe('Test Get Sales Representatives', function(){
  describe('Test Get All Sales Representatives end point', function() {
    describe("Positive Cases", function() {
        it('Should Return all Sales Representatives in JSON format', function(done){
          request.get({url: baseUrl + paths.salesPerson}, function(error, response, body) {
          //console.log(baseUrl);
          var bodyObj = JSON.parse(body);
          expect(response.statusCode).to.equal(404);
          //console.log(body);
          done();
          });
        }).timeout(0);
    }); //Close Positive describe

    describe("Negative Cases", function(){
      it('Should return status response 404', function(done){
        request.get({url: baseUrl + paths.salesPerson}, function(error, response, body) {
        expect(response.statusCode).to.equal(404);
        //console.log(body);
        done();
        });
      }).timeout(0);

      it('Should fail TC: validating status response 200', function(done){ //Done on purpose to show a failure
        request.get({url: baseUrl + paths.salesPerson}, function(error, response, body) {
        expect(response.statusCode).to.equal(200);
        //console.log(body);
        done();
        });
      }).timeout(0);
    });
  });

  describe('Get Sales Representatives by ID endpoint (/sales-person/{ID})', function() {
    describe("Positive Cases", function() {
        it('Should return Error Message: Resource not found', function(done){
          request.get({url: baseUrl + paths.salesPerson}, function(error, response, body) {
            var bodyObj = JSON.parse(body);
            expect(bodyObj.message).to.equal(parameters.salesPerson.NotFound.message);
            expect(response.statusCode).to.equal(404);
            //console.log(body);
            done();
          });
        }).timeout(0);
    });
  });
});
