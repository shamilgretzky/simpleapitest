//define some variables
var should = require("should");
var request = require("request");
var expect = require("chai").expect;
var util = require("util");

const config = require("../config/config");

let paths = require("../resources/paths.json");
let parameters = require("../resources/parameters.json");
let baseUrl = config.baseUrl;

describe('Test Get Payment Methods', function(){
  describe('Test Get All Payment Methods end point', function() {
    describe("Positive Cases", function() {
        it('Should Return all Payment Methods in JSON format', function(done){
          request.get({url: baseUrl + paths.paymentMethods}, function(error, response, body) {
          //console.log(baseUrl);
          var bodyObj = JSON.parse(body);
          expect(response.statusCode).to.equal(200);
          //console.log(body);
          done();
          });
        }).timeout(0);

        it('Returns all Payment Methods with status response 200', function(done){
          request.get({url: baseUrl + paths.paymentMethods}, function(error, response, body) {
          expect(response.statusCode).to.equal(200);
          //console.log(body);
          done();
          });
        }).timeout(0);
    }); //Close Positive describe

    describe("Negative Cases", function(){
          it("Should return status response 404 for passing an invalid path",function(done){
            request.get({url: baseUrl + paths.invalidPaymentMethods}, function(error, response, body) {
            //console.log(body);
            expect(response.statusCode).to.equal(404);
            //console.log(body);
            done();
            });
          }).timeout(0);
    });
  });

  describe('Get Payment Methods by ID endpoint (/payment-methods/{ID})', function() {
    describe("Positive Cases", function() {
        it('Should Return corresponding Payment Methods when passing specific id', function(done){
          request.get({url: baseUrl + paths.paymentMethods + parameters.paymentMethods.Payment1.id}, function(error, response, body) {
            var bodyObj = JSON.parse(body);
            expect(bodyObj.id).to.equal(parameters.paymentMethods.Payment1.id);
            expect(response.statusCode).to.equal(200);
            //console.log(body);
            done();
          });
        }).timeout(0);

        it('Should return correct Payment Methods name in the response body', function(done){
          request.get({url: baseUrl + paths.paymentMethods + parameters.paymentMethods.Payment1.id}, function(error, response, body) {
            var bodyObj = JSON.parse(body);
            expect(bodyObj.name).to.equal(parameters.paymentMethods.Payment1.name);
            expect(response.statusCode).to.equal(200);
            //console.log(body);
            done();
          });
        }).timeout(0);
      });

      describe("Negative Cases", function() {
        it('Should return null (Passing Invalid Parameter for Payment Methods)', function(done){
          request.get({url: baseUrl + paths.paymentMethods + parameters.paymentMethods.InvalidPayment.id}, function(error, response, body) {
            var bodyObj = JSON.parse(body);
            expect(bodyObj).to.not.equal(parameters.paymentMethods.InvalidPayment.id);
            expect(response.statusCode).to.equal(200);
            //console.log(body);
            done();
          });
        }).timeout(0);
      });
  });
});
